package com.ensta.rentmanager.service;

import java.util.List;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.model.Vehicle;
import com.ensta.rentmanager.dao.ClientDao;
import com.ensta.rentmanager.dao.VehicleDao;

public class VehicleService {

	// TODO: Bonus: implementer le design pattern singleton pour cette classe
	// TODO: Bonus: implementer un mecanisme injectant les daos necessaire pour ce service
		
		
	public long create(Vehicle vehicle) throws ServiceException {
		// TODO: Creer un vehicule
		return 0;
	}

	public Vehicle findById(long id) throws ServiceException {
		// TODO: Recuperer un vehicule par son id
		return null;
	}

	public List<Vehicle> findAll() throws ServiceException {
		// TODO: Recuperer tous les vehicules
		return null;
	}
	
}
