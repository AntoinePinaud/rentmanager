package com.ensta.rentmanager.service;

import java.util.List;

import com.ensta.rentmanager.exception.DaoException;
import com.ensta.rentmanager.exception.ServiceException;
import com.ensta.rentmanager.model.Client;
import com.ensta.rentmanager.dao.ClientDao;

public class ClientService {

	// TODO: Bonus: implementer le design pattern singleton pour cette classe
	// TODO: Bonus: implementer un mecanisme injectant les dao necessaire pour ce service
		
	
	public long create(Client client) throws ServiceException {
		// TODO: Creer un client
		return 0;
	}

	public Client findById(long id) throws ServiceException {
		// TODO: Recuperer un client par son id
		return null;
	}

	public List<Client> findAll() throws ServiceException {
		// TODO: Recuperer tous les clients
		return null;
	}
	
}
